#!/usr/bin/python3
"""Set up direct route to jitsi.nic.cz on VPN start.

Usage: routes.py [options] [<args>...]
       routes.py -h | --help

Options:
  -h, --help           show this help message and exit
  -n, --dry-run        just print route commands, do not run them
  -d, --device=DEVICE  specify device to be used, use first wifi by default.
"""
import json
import subprocess  # nosec
import time
from typing import Iterable, Optional

from docopt import docopt


def get_jitsi_address(record: str = 'A') -> str:
    """Return jitsi.nic.cz IP address."""
    assert record in ('A', 'AAAA')  # nosec
    result = subprocess.run(['dig', record, 'jitsi.nic.cz', '+nocomments', '+short'], check=True,  # nosec
                            stdout=subprocess.PIPE)
    return result.stdout.splitlines()[-1].decode()


def get_default_device() -> str:
    """Return default device name."""
    result = subprocess.run(['ip', '-json', 'addr', 'show', 'up'], check=True, stdout=subprocess.PIPE)  # nosec
    for interface in json.loads(result.stdout):
        device_name = interface['ifname']
        if device_name.startswith('wl'):
            return device_name
    raise ValueError('Default device not found.')


def get_default_gateway(device: str, ip_version: int = 4) -> Optional[str]:
    """Return default route data."""
    assert ip_version in (4, 6)  # nosec
    result = subprocess.run(  # nosec
        ['ip', '-{}'.format(ip_version), '-json', 'route', 'show', 'default', 'dev', device],
        check=True, stdout=subprocess.PIPE)
    routes = json.loads(result.stdout)
    if not routes:
        return None
    return routes[0]['gateway']


def run_command(command: Iterable[str], *, check: bool = True, dry_run: bool = False) -> None:
    """Run or dry-run a command."""
    if dry_run:
        print(' '.join(command))
    else:
        subprocess.run(command, check=check)  # type: ignore # nosec


def set_routes() -> None:
    """Run the script to set up routes."""
    options = docopt(__doc__)

    device = options['--device'] or get_default_device()

    # Run the default /etc/openvpn/update-resolv-conf
    run_command(['/etc/openvpn/update-resolv-conf'] + options['<args>'], dry_run=options['--dry-run'])

    # Wait a second for the update-resolv-conf to catch up.
    time.sleep(1)

    run_command(['ip', 'route', 'add', get_jitsi_address(), 'via', get_default_gateway(device)],
                check=False, dry_run=options['--dry-run'])
    gateway_v6 = get_default_gateway(device, 6)
    if gateway_v6:
        run_command(
            ['ip', 'route', 'add', get_jitsi_address('AAAA'), 'via', gateway_v6, 'dev', device],
            check=False, dry_run=options['--dry-run'])


if __name__ == '__main__':
    set_routes()
