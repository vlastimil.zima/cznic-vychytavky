Various scripts for CZ.NIC
==========================

routes.py
---------

Script to set up route to jitsi.nic.cz directly and avoid routing through VPN.
